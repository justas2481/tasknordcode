<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Form\FormError;
use App\Entity\Task;
use App\Entity\User;
use App\Form\TaskFormType;
use App\Form\TasksReportFormType;
use App\Report\{CSVReporter, XLSXReporter, PDFReporter};

class TaskController extends AbstractController
{
    /**
     * Security for the User
     *
     * @var Security
     */
    private $security;

    /**
     *
     * @access public
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/new-task", name="task")
     */
    public function new(Request $request): Response
    {
        $task = new Task();
        $form = $this->createForm(TaskFormType::class, $task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->security->getUser();
            if (empty($user)) {
                return $this->redirectToRoute('app_login');
            }

            $task->setUser($user);
            $task->setDate(Task::parseDate($form->get('date')->getData()));
            $task->setTimeSpent((int) $form->get('timeSpent')->getData());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('task/index.html.twig', [
            'controller_name' => 'TaskController',
            'task_form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/tasks-report", name="tasks_report")
     */
    public function report(Request $request, Security $security): Response
    {
        $form = $this->createForm(TasksReportFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$this->isGranted('ROLE_USER')) {
                return $this->redirectToRoute('app_login');
            }

            $reportData = $this->getDoctrine()->getRepository(Task::class)->calculateReport(
                $security->getUser(),
                Task::parseDate($form->get('date_from')->getData()),
                Task::parseDate($form->get('date_to')->getData())
            );
            if (empty($reportData)) {
                $form->addError(new FormError('There is nothing to generate between specified dates.'));
                return $this->render('task/report.html.twig', [
                    'controller_name' => 'TaskController',
                    'tasks_report_form' => $form->createView(),
                ]);
            }

            $totalTimeInMinutes = Task::calcReportTime($reportData);
            $fileDir = $this->getParameter('kernel.project_dir') . '/public/report_files/';
            $format = $form->get('format')->getData();
            $extension = '.csv'; // Default

            switch ($format) {
                case 'csv':
                    $extension = '.csv';
                    $reporter = new CSVReporter($fileDir . 'report' . $extension, $reportData, $totalTimeInMinutes);
                    $contentType = 'text/csv';
                    break;
                case 'xlsx':
                    $extension = '.xlsx';
                    $reporter = new XLSXReporter($fileDir . 'report' . $extension, $reportData, $totalTimeInMinutes);
                    $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    break;
                case 'pdf':
                    $extension = '.pdf';
                    $reporter = new PDFReporter($fileDir . 'report' . $extension, $reportData, $totalTimeInMinutes);
                    $contentType = 'application/pdf';
                    break;

                default:
                    return $this->redirectToRoute('home');
            }

            $reporter->generateFile();
            $response = new Response(file_get_contents($fileDir . 'report' . $extension));
            $response->headers->set('Content-Type', $contentType);
            $response->headers->set('Content-Disposition', 'attachment; filename=report' . $extension);

            return $response;
        }

        return $this->render('task/report.html.twig', [
            'controller_name' => 'TaskController',
            'tasks_report_form' => $form->createView(),
        ]);
    }
}
