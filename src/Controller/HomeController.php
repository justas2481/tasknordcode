<?php

namespace App\Controller;

use App\Repository\TaskRepository;
use App\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class HomeController extends AbstractController
{
    /**
     * @Route("/")
     * @Route("/home", name="home")
     */
    public function index(Request $request, Security $security): Response
    {
        $tasks = [];
        $offset = max(0, $request->query->getInt('offset', 0));
        if ($this->isGranted('ROLE_USER')) {
            $tasks = $this->getDoctrine()->getRepository(Task::class)->findByUserId($security->getUser(), $offset);
        }

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'tasks' => $tasks,
            'previous' => $offset - TaskRepository::ITEMS_PER_PAGE,
            'next' => min(count($tasks), $offset + TaskRepository::ITEMS_PER_PAGE),
        ]);
    }
}
