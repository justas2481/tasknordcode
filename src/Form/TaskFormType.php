<?php

namespace App\Form;

use App\Entity\Task;
use Doctrine\DBAL\Types\BigIntType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\{TextType, TextareaType, NumberType, SubmitType};

class TaskFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Task title',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a title',
                    ]),
                    new Length([
                        'min' => 10,
                        'minMessage' => 'Title should be at least {{ limit }} characters',
                        'max' => 100,
                        'maxMessage' => 'Title is to long',
                    ]),
                ]
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'Comment',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a comment',
                    ]),
                    new Length([
                        'min' => 20,
                        'minMessage' => 'Comment should be at least {{ limit }} characters',
                        'max' => 255,
                        'maxMessage' => 'Comment is to long',
                    ]),
                ]
            ])
            ->add('date', TextType::class, ['mapped' => false])
            ->add('timeSpent', NumberType::class)
            ->add('submit', SubmitType::class, ['label' => 'Publish']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);
    }
}
