<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\{TextType, DateType, ChoiceType, SubmitType};

class TasksReportFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date_from', TextType::class)
            ->add('date_to', TextType::class)
            ->add('format', ChoiceType::class, ['choices' => ['XLSX' => 'xlsx', 'CSV' => 'csv', 'PDF' => 'pdf'], 'label' => 'File format'])
            ->add('submit', SubmitType::class, ['label' => 'Calculate report'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
