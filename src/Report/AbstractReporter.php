<?php

namespace App\Report;

abstract class AbstractReporter
{
    protected $filePath;
    protected $data;
    protected $totalTimeInMinutes;

    public function __construct(string $filePath, array &$reportData, int $timeInMinutes = 0)
    {
        $this->setFilePath($filePath);
        $this->setReportData($reportData);
        $this->totalTimeInMinutes = $timeInMinutes;
    }

    public function setFilePath(string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function setTimeInMinutes($time): self
    {
        $this->totalTimeInMinutes = $time;

        return $this;
    }

    public function setReportData(array &$data): self
    {
        $this->reportData = $data;

        if (empty($this->reportData)) {
            throw new \InvalidArgumentException('Import data can not be empty');
        }

        return $this;
    }

    abstract public function generateFile(): bool;
}
