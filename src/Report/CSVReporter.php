<?php

namespace App\Report;

class CSVReporter extends AbstractReporter
{
    public function generateFile(): bool
    {
        if (!$file = fopen($this->filePath, 'w')) {
            throw new \Exception('Unable to open file for writeing');
        }

        fputcsv($file, ['Title', 'Comment', 'Date', 'Time spent']);
        foreach ($this->reportData as $task) {
            fputcsv($file, [
                $task->getTitle(),
                $task->getComment(),
                $task->getDate()->format('Y-m-d'),
                $task->getTimeSpent()
            ]);
        }
        fputcsv($file, ['Time total:', $this->totalTimeInMinutes]);
        fclose($file);

        return true;
    }
}
