<?php

namespace App\Report;

use Dompdf\Dompdf;
use Dompdf\Options;

class PDFReporter extends AbstractReporter
{
    public function generateFile(): bool
    {
        $htmlData = <<<EOT
        <html>
        <head>
        <meta charset="utf-8">
        <title>Report</title>
        </head>
        <body>
        <table>
EOT; // Yes, there can be Twig template
        foreach ($this->reportData as $task) {
            $htmlData .= '<tr><td>' . $task->getTitle() . '</td><td>' .
                $task->getComment() . '</td><td>' .
                $task->getDate()->format('Y-m-d') . '</td><td>' .
                $task->getTimeSpent() . '</td></tr>';
        }
        $htmlData .= '<tr>
        <td>Total time:</td>
        <td>' . $this->totalTimeInMinutes . '</td>
        </tr>
        </table>
        </body>
        </html>';
        $options = new Options();
        $options->set('defaultFont', 'Arial');
        $dompdf = new Dompdf($options);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->load_html($htmlData);
        $dompdf->render();
        file_put_contents($this->filePath, $dompdf->output()); // This pdf lib generates content in a horible way though should be enough to demonstrate that I can do this

        return true;
    }
}
