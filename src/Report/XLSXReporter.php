<?php

namespace App\Report;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class XLSXReporter extends AbstractReporter
{
    public function generateFile(): bool
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Title');
        $sheet->setCellValue('B1', 'Comment');
        $sheet->setCellValue('C1', 'Date');
        $sheet->setCellValue('D1', 'Time spent');
        $row = 2;
        foreach ($this->reportData as $task) {
            $letter = 'A';
            $cols = [
                $task->getTitle(),
                $task->getComment(),
                $task->getDate()->format('Y-m-d'),
                $task->getTimeSpent()
            ];
            foreach ($cols as $col) {
                $sheet->setCellValue($letter . $row, $col);
                ++$letter;
            }
            ++$row;
        }
        $sheet->setCellValue('A' . $row, 'Time total:');
        $sheet->setCellValue('B' . $row, $this->totalTimeInMinutes);

        $writer = new Xlsx($spreadsheet);
        $writer->save($this->filePath);

        return true;
    }
}
