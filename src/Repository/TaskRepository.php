<?php

namespace App\Repository;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    /**
     * How many items to show by using Paginator by default
     */
    public const ITEMS_PER_PAGE = 10;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    /**
     * Get all tasks that belong to the current user
     *
     * @param User $user
     * @param integer $offset Which item should be first from which to show result set
     * @param integer $itemsPerPage
     * @return Paginator - collection of tasks that belong to the User
     */
    public function findByUserId(User $user, int $offset = 0, int $itemsPerPage = self::ITEMS_PER_PAGE): Paginator
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.user = :userId')
            ->setParameter('userId', $user->getId())
            ->orderBy('t.date', 'DESC')
            ->setMaxResults($itemsPerPage)
            ->setFirstResult($offset)
            ->getQuery();

        return new Paginator($query);
    }

    public function calculateReport(User $user, \DateTimeInterface $dateFrom, \DateTimeInterface $dateTo): array
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.user = :userId')
            ->andWhere('t.date BETWEEN :dateFrom AND :dateTo')
            ->setParameter('userId', $user->getId())
            ->setParameter('dateFrom', $dateFrom->format('Y-m-d'))
            ->setParameter('dateTo', $dateTo->format('Y-m-d'))
            ->orderBy('t.date', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Task
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
